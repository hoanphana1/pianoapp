package com.hoanphan.piano.ultil;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.util.SparseIntArray;

public class SoundManager {
    private SoundPool mSoundPool;
    private SparseIntArray mSoundPoolMap;

    private Handler mHandler;
    private boolean mMuted;

    private static Context mContext;

    private static final int MAX_STREAMS  = 10;
    private static final int STOP_DELAY_MILLIS = 10000;

    private static SoundManager instance = null;

    public SoundManager(){

        mSoundPoolMap = new SparseIntArray();
        mHandler = new Handler();
        mSoundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
    }
    //singleton
    public static SoundManager getInstance(){
        if(instance == null){

            instance = new SoundManager();
        }
        return instance;
    }
    public void init(Context context){
        this.mContext = context;
    }
    public static void initStreamTypeMedia(Activity activity){
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }
    public void addSound(int soundID){
        mSoundPoolMap.put(soundID,mSoundPool.load(mContext,soundID,1));
    }
    public void playSound(int soundID){
        if(mMuted){
            return;
        }
        boolean hasSound = mSoundPoolMap.indexOfKey(soundID) >=0;
        if(!hasSound){
            return;
        }
        final int sound = mSoundPool.play(mSoundPoolMap.get(soundID),1,1,1,0,1);

        scheduleSoundStop(sound);
    }
    public void scheduleSoundStop(final int soundID){
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSoundPool.stop(soundID);
            }
        },STOP_DELAY_MILLIS);
    }
}
