package com.hoanphan.piano.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.hoanphan.piano.model.Key;
import com.hoanphan.piano.ultil.SoundManager;

import java.util.ArrayList;

public class PianoView extends View {
    private static final int NUMBER_OF_KEY = 14;
    private Paint white, black,yellow;
    private ArrayList<Key> whites,blacks;
    private int keyWidth, keyHeight;
    private SoundManager soundManager;

    public PianoView(Context context, @Nullable AttributeSet attrs) {

        super(context, attrs);
        black = new Paint();
        black.setColor(Color.BLACK);
        black.setStyle(Paint.Style.FILL);

        white= new Paint();
        white.setColor(Color.WHITE);
        white.setStyle(Paint.Style.FILL);

        yellow = new Paint();
        yellow.setColor(Color.YELLOW);
        yellow.setStyle(Paint.Style.FILL);

        whites = new ArrayList<Key>();
        blacks = new ArrayList<Key>();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        keyWidth = w / NUMBER_OF_KEY;
        keyHeight = h;

        int countBlackKey = 15;
        //khoi tao mang key
        for(int i = 0; i < NUMBER_OF_KEY ; i++){
            int left = i*keyWidth;
            int right = left + keyWidth;

            //neu la phim trang cuoi cung
            if(i == NUMBER_OF_KEY - 1){
                right = w;
            }
            RectF rect = new RectF(left, 0, right,h );
            whites.add(new Key(i+1,rect,false));

            //khoi tao mang phim den
            if( i!=0 && i!= 3 && i!=7 && i!=10){
                rect = new RectF((float)(i-1)*keyWidth + keyWidth*0.75f,
                                0,
                            (float)i*keyWidth + keyWidth*0.25f,
                        0.67f*keyHeight);
                blacks.add(new Key(countBlackKey++, rect,false));
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //ve 14 phim trang
        for(Key key:whites){
            canvas.drawRect(key.rect, key.down ? yellow : white);
        }
        //ve 13 dg chia cac phim trang
        for(int i = 1 ; i < NUMBER_OF_KEY; i++){
            canvas.drawLine(i*keyWidth,0,i*keyWidth,keyHeight,black);

        }
        //ve phim den
        for(Key key:blacks){
            canvas.drawRect(key.rect, key.down ? yellow : black);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        boolean isDownAction = action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE;

        for (int touchIndex = 0; touchIndex<event.getPointerCount();touchIndex++){
            float x = event.getX();
            float y = event.getY();




            boolean isBlackPressed=false;
            for(Key k: blacks) k.down = false;
            for(Key k: whites) k.down = false;

            for(Key k: blacks) if(k.rect.contains(x,y)) {k.down = isDownAction; isBlackPressed=true;}
            for(Key k: whites) if(k.rect.contains(x,y)&&!isBlackPressed) k.down = isDownAction;





        }
        this.invalidate();
        return super.onTouchEvent(event);
    }
}
